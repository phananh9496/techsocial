import React from 'react';
import logo from "../images/logo.png";


const Footer = () => (
	<div className="ft-bg mt-5">
		<div className="container">
			<div className="row">
				<div className="col-4 mt-5 text-center">
					<img src={`${logo}`} alt="" />
				</div>
				<div className="col-2 mt-5 text-center">

				</div>
				<div className="col-6 mt-5 contact">
					<h2>Team</h2>
					<p>Nhóm thực hành chuyên sâu:</p>
					<p>Phan Anh: B14DCPT207</p>
					<p>Lăng Hồng Sơn: B14DCPT207</p>
					<p>Địa chỉ: Km10 Đường Nguyễn Trãi, 96 Trần Phú, P. Mộ Lao, Hà Đông, Hà Nội</p>
				</div>
			</div>
		</div>
	</div>
)

export default Footer;
