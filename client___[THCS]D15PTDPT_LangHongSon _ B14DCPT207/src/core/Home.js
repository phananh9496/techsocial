import React from 'react';
import Posts from "../post/Posts";
import tech from "../images/tech.jpg";

const Home = () => (
	<div>
		<div id="demo" className="carousel slide mt-2" data-ride="carousel">
			<div className="carousel-item active banner">
		    	<img src={`${tech}`} alt="Los Angeles"/>
		    	<div className="carousel-caption caro-cap">
			        <h2>Tech Social</h2>
			        <p>sharing technology knowledge</p>
		      	</div> 
		    </div>
 		</div>
		<div className="container">
	        <Posts />
	    </div>
	</div>
);

export default Home;