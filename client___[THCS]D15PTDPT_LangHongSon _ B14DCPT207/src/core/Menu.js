import React from 'react'
import {Link, withRouter} from 'react-router-dom'
import {signout, isAuthenticated} from '../auth/index'
import logo from "../images/logo.png";

const isActive = (history, path) => {
	if (history.location.pathname === path) {
		return {color : '#ff9100'};
	}
	else {
		return {color : '#ffffff'};
	}
}


const Menu = ({history}) => (
	<div>
		<ul className="nav nav-tabs shadow bg-white">
			<li className="nav-item nav-logo mr-auto p-2">
				<Link className="nav-link" style={isActive(history, "/")} to="/">
					<img src={`${logo}`} alt="" />
				</Link>
			</li>

			<li className="nav-item mt-4">
				<Link className="nav-link text-dark font-weight-bold" style={isActive(history, "/users")} to="/users">
					Users
				</Link>
			</li>


			<li className="nav-item mt-4">
                <Link
                    to={`/post/create`}
                    style={isActive(history, `/post/create`)}
                    className="nav-link text-dark font-weight-bold"
                >
                    Tạo Bài Viết
                </Link>
            </li>

			{!isAuthenticated() && (
				<div class="d-flex justify-content-sm-end">	
					<li className="nav-item mt-4">
						<Link className="nav-link text-dark font-weight-bold" style={isActive(history, "/signin")} to="/signin">
							Sign In
						</Link>
					</li>
					<li className="nav-item mt-4">
						<Link className="nav-link text-dark font-weight-bold" style={isActive(history, "/signup")} to="/signup">
							Sign Up
						</Link>
					</li>
				</div>
			)}

			{isAuthenticated() && (
				<>

					<li className="nav-item mt-4">
						<Link to={`/user/${isAuthenticated().user._id}`}
						style={isActive(history, `/user/${isAuthenticated().user._id}`)} className="nav-link text-dark font-weight-bold">
							{`Profile: ${isAuthenticated().user.name}`}
						</Link>
					</li>

					<li className="nav-item mt-4">
						<span className="nav-link text-dark font-weight-bold" style={isActive(history, "/signup"), {cursor:"pointer", color:"#fff"}} 
							onClick={() => signout(() => history.push("/"))}>
								Sign Out
						</span>
					</li>
				</>
			)}
				
		</ul>
	</div>
);

export default withRouter(Menu);


